import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LinearProgress from '@material-ui/core/LinearProgress';
import Link from 'next/link';
import { Query, withApollo } from 'react-apollo';
import { GET_POSTS } from '../lib/graphql';

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

function SimpleTable(props) {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>Title</TableCell>
            <TableCell align="right">Votes</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <Query query={GET_POSTS}>
            {({ loading, data: { allPosts } }) => {
              if (loading) return (
                <TableRow>
                  <TableCell colSpan={3}>
                    <LinearProgress />
                  </TableCell>
                </TableRow>
              )

              return allPosts.map(post => (
                <TableRow key={post.id}>
                  <TableCell>{post.id}</TableCell>
                  <TableCell>
                    <Link href={post.url} passHref>
                      <a target="_blank">{post.title}</a>
                    </Link>
                  </TableCell>
                  <TableCell align="right">{post.votes}</TableCell>
                </TableRow>
              ))
            }}
          </Query>
        </TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
  first: PropTypes.number.isRequired 
};

const Posts = withStyles(styles)(SimpleTable);
export default withApollo(Posts)