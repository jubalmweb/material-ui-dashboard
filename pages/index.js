import { Typography } from "@material-ui/core";
import App from "../components/App";
import PostsTable from "../components/PostsTable"

export default () => (
  <App title="Dashboard">
    <Typography variant="h5" gutterBottom component="h2">
      Latest Posts
    </Typography>
    <PostsTable />
  </App>
);
