# Material Dashboard

Quickly setup a backend UI for your GraphQL Application with React + SSR via NextJS, Material UI, and Apollo GraphQL Client

### Bonus Tip

Quickly deploy this app to [now.sh](https://zeit.co/now) with a single cli command.