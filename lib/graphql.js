import gql from "graphql-tag";

export const GET_POSTS = gql`
  {
    allPosts(first: 5) {
      id
      votes
      title
      url
      votes
    }
  }
`;
